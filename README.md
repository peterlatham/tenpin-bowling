# Tenpin bowling

This requires [node.js](https://nodejs.org/en/) installed and is written in [typescript](https://www.typescriptlang.org/) 


## Install 

```sh
npm install
```

## Build 

- includes linting

```sh
npm run build
```

## Test 

Test cases are expressed in [Chai](https://www.chaijs.com) and executed using [Mocha](https://mochajs.org) and can be run with the following coommand 

```sh
npm run test
```

## Running

```sh
node dist/bowling.js --file ${pathToResultsFile}
```

### Required arguments

--file, alias -f 

The path to the file which contains a valid sequence of rolls for one line of American Ten-Pin Bowling

example
```sh
--f ./bowling-scores.txt
```

### Input format

```text
2, 3, 5, 4, 9, 1, 2, 5, 3, 2, 4, 2, 3, 3, 4, 6, 10, 3, 2
```

### Output format 

```sh
| f1 | f2 | f3 | f4 | f5 | f6 | f7 | f8 | f9 | f10   |
|roll1[, roll2]|roll1[, roll2]|...|roll1[, roll2][, roll3]|
score: score
```

## Assumptions 

- The input file has valid content. (no validity checking)
- The input file contains exactly the results for one game of bowling for one player - that is exactly one line. 