class Frame {
  protected order: number;
  protected firstRoll!: number;
  protected secondRoll!: number;
  protected MAX_ROLLS: number = 2;
  protected numberOfRolls: number = 0;
  protected MAX_PINS: number = 10;
  protected remainingPins = this.MAX_PINS;
  readonly SPARE = "/";
  readonly STRIKE = "X";
  readonly MISS = "-";

  constructor (order: number) {
    this.order = order;
  }

  public canReceiveRolls(): boolean {
    return this.numberOfRolls < this.MAX_ROLLS;
  }

  public isStrike(): boolean {
    return this.firstRoll === this.MAX_PINS;
  }

  public isSpare(): boolean {
    return (this.firstRoll + this.secondRoll === this.MAX_PINS && this.secondRoll !== undefined);
  }

  public inputRollScore(score: number) {
    this.checkInput(score);
    this.updateFrame(score);
    this.numberOfRolls++;
  }

  public getFirstRoll(): number {
    return this.firstRoll;
  }

  public getSecondRoll(): number {
    return this.secondRoll;
  }

  public printFrameHeader(): string {
    return ` f${this.order} |`;
  }

  public printFrameContent(): string {
    return `${this.printFirstRoll()}${this.printSecondRoll()}|`;
  }

  protected checkInput(score: number) {
    if (score > this.MAX_PINS) throw new Error(`The score is higher than max number of pins ${this.MAX_PINS}`);
    if (!this.canReceiveRolls()) throw new Error(`The frame has received max rolls of ${this.MAX_ROLLS} `);
  }

  protected updateFrame(score: number) {
    if (this.firstRoll === undefined) {
      this.firstRoll = score;
      this.remainingPins -= score;
      if (this.isStrike()) this.numberOfRolls++;
    } else {
      if (score > this.remainingPins) {
        throw new Error(`Can not throw a score of ${score} when there are only ${this.remainingPins} pins remaining`);
      }
      this.secondRoll = score;
      this.remainingPins -= score;
    }
  }

  protected printFirstRoll(): string {
    if (this.firstRoll === undefined) return ` `;
    if (this.isStrike()) return `${this.STRIKE}`;
    if (this.firstRoll === 0) return `${this.MISS}`;
    return `${this.firstRoll}`;
  }

  protected printSecondRoll(): string {
    if (this.secondRoll === undefined) return `   `;
    if (this.secondRoll === 0) return `, ${this.MISS}`;
    if (this.isSpare()) return `, ${this.SPARE}`;
    return `, ${this.secondRoll}`;
  }
}

export { Frame };