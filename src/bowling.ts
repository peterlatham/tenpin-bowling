import * as yargs from "yargs";
import * as fs from "fs";
import { bowl } from "./scoreboard";

const args = yargs
  .option("file", {
    alias: "f",
    description: "Bowling Results file",
    demand: true
  }).argv;

const resultsFileString = (args.file) ? fs.readFileSync(args.file as string).toString() : "";

const { headers, results, total } = bowl(resultsFileString);
console.log(headers);
console.log(results);
console.log(total);