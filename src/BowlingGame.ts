import { Frame } from "./Frame";
import { FinalFrame } from "./FinalFrame";

class BowlingGame {

  private MAX_FRAMES: number = 10;
  private frames: Frame[] = [];

  constructor() {
    for (let i = 0; i < this.MAX_FRAMES - 1; i++) {
      this.frames.push(new Frame(i + 1));
    }
    this.frames.push(new FinalFrame(this.MAX_FRAMES));
  }

  public fillScores (scores: string[]) {
    for (const frame of this.frames) {
      while (frame.canReceiveRolls() && scores.length > 0) {
        const score: string = scores.shift()!;
        frame.inputRollScore(+score);
      }
    }
  }

  public getResults(): BowlingGameResults {
    let headers = `|`;
    let content = `|`;

    for (const frame of this.frames) {
      headers += frame.printFrameHeader();
      content += frame.printFrameContent();
    }
    const score: number = this.scoreFrames();

    return {
      headers: headers,
      results: content,
      score: score,
      total: `score: ${score}`
    };
  }

  public scoreFrames(): number {
    let total: number = 0;
    // caculate all the normal Frame scores.
    for (let index = 0; index < this.frames.length - 1; index++) {
      const frame = this.frames[index];
      if (frame.getFirstRoll() === undefined) break;
      total += frame.getFirstRoll();
      if (frame.isStrike()) {
        total += this.addScoringBonus(index, ScoringBonus.STRIKE);
        continue; // no need to calculate second roll for this frame - it's undefined
      }
      if (frame.getSecondRoll() === undefined) break;
      total += frame.getSecondRoll();
      if (frame.isSpare()) total += this.addScoringBonus(index,  ScoringBonus.SPARE);
    }
    // add in the Final Frame
    const finalFrame: FinalFrame = this.frames[this.frames.length - 1] as FinalFrame;
    total += finalFrame.getFrameScore();
    return total;
  }

  private addScoringBonus(frameIndex: number, bonus: ScoringBonus): number {
    let bonusScore: number = this.firstExtraRoll(frameIndex);
    if (bonus === ScoringBonus.STRIKE) bonusScore += this.secondExtraRoll(frameIndex);
    return bonusScore;
  }

  private firstExtraRoll(index: number) {
    return this.frames[index + 1].getFirstRoll();
  }

  private secondExtraRoll(index: number) {
    return this.frames[index + 1].getSecondRoll() || this.frames[index + 2].getFirstRoll();
  }
}

enum ScoringBonus {
  STRIKE,
  SPARE
}
interface BowlingGameResults {
  headers: string;
  results: string;
  total: string;
  score: number;
}

export { BowlingGame, BowlingGameResults };
