import { Frame } from "./Frame";

class FinalFrame extends Frame {
  private thirdRoll!: number;
  protected MAX_ROLLS: number = 3;
  private frameScore: number;

  constructor(order: number) {
    super(order);
    this.frameScore = 0;
  }

  // this special frame can calculate it's own score
  public getFrameScore(): number {
    return this.frameScore;
  }

  public printFrameHeader(): string {
    return ` f${this.order}   |`;
  }

  public printFrameContent(): string {
    return `${this.printFirstRoll()}${this.printSecondRoll()}${this.printThirdRoll()}|`;
  }

  protected updateFrame(score: number) {
    if (this.firstRoll === undefined) {
      this.firstRoll = score;
      if (!this.isStrike()) this.remainingPins -= score;
    } else if (this.secondRoll === undefined) {
      if (score > this.remainingPins) {
        throw new Error(`Can not throw a score of ${score} when there are only ${this.remainingPins} pins remaining`);
      }
      this.secondRoll = score;
      this.remainingPins -= score;
      if (this.remainingPins > 0 && !this.isStrike()) this.numberOfRolls++;
      if (this.isSpare() || this.isSecondRollStrike()) this.remainingPins = this.MAX_PINS;
    } else {
      if (score > this.remainingPins) {
        throw new Error(`Can not throw a score of ${score} when there are only ${this.remainingPins} pins remaining`);
      }
      this.thirdRoll = score;
    }
    this.frameScore += score;
  }

  protected printSecondRoll(): string {
    if (this.secondRoll === undefined) return `   `;
    if (this.secondRoll === 0) return `, ${this.MISS}`;
    if (this.isSecondRollStrike()) return `, ${this.STRIKE}`;
    if (this.isSpare()) return `, ${this.SPARE}`;
    return `, ${this.secondRoll}`;
  }

  protected printThirdRoll(): string {
    if (this.thirdRoll === undefined) return `   `;
    if (this.thirdRoll === 0) return `, ${this.MISS}`;
    if (this.isThirdRollSpare()) return `, ${this.SPARE}`;
    if (this.thirdRoll === this.MAX_PINS) return `, ${this.STRIKE}`;
    return `, ${this.thirdRoll}`;
  }

  private isSecondRollStrike(): boolean {
    return this.secondRoll === this.MAX_PINS && this.isStrike();
  }

  private isThirdRollSpare(): boolean {
    return this.isStrike() && !this.isSecondRollStrike() && this.secondRoll + this.thirdRoll === this.MAX_PINS;
  }
}

export { FinalFrame };