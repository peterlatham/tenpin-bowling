import * as mocha from "mocha";
import chai from "chai";
import { frameTests } from "./suites/frame.test";
import { gameResultsTests } from "./suites/game.test";
import { headerTests } from "./suites/header.test";
import { scoreTests } from "./suites/score.test";
import { finalFrameTests } from "./suites/final-frame.test";

const expect = chai.expect;

describe("Let's bowl ", function() {
  this.timeout(2500);
  before( "init tests ", () => {
    // setup
  });
  after("close tests", () => {
    // cleanup
  });

  headerTests();
  frameTests();
  finalFrameTests();
  gameResultsTests();
  scoreTests();
});
