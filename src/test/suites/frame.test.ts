import * as mocha from "mocha";
import chai from "chai";
import { Frame } from "../../Frame";

const expect = chai.expect;

const frameTests = function() {
  describe("Frame Tests", function() {
    it("Frame: print a header", function() {
      const frame = new Frame(5);
      const header = frame.printFrameHeader();
      expect(header).to.be.a("string");
      expect(header).to.eq(" f5 |");
    });

    it("Frame: isStrike()", function() {
      const frame = new Frame(1);
      frame.inputRollScore(10);
      expect(frame.isStrike()).to.be.a("boolean");
      expect(frame.isStrike()).to.eq(true);
      const badFrame = new Frame(3);
      badFrame.inputRollScore(0);
      badFrame.inputRollScore(10);
      expect(badFrame.isStrike()).to.eq(false);
    });

    it("Frame: isSpare()", function() {
      const frame = new Frame(2);
      frame.inputRollScore(10);
      expect(frame.isSpare()).to.be.a("boolean");
      expect(frame.isSpare()).to.eq(false);
      const spareFrame = new Frame(3);
      spareFrame.inputRollScore(0);
      spareFrame.inputRollScore(10);
      expect(spareFrame.isSpare()).to.eq(true);
      const anotherSpareFrame = new Frame(3);
      anotherSpareFrame.inputRollScore(4);
      anotherSpareFrame.inputRollScore(6);
      expect(spareFrame.isSpare()).to.eq(true);
    });

    it("Frame: Can recieve rolls", function() {
      const frame = new Frame(1);
      frame.inputRollScore(4);
      expect(frame.canReceiveRolls()).to.be.a("boolean");
      expect(frame.canReceiveRolls()).to.eq(true);
      frame.inputRollScore(5);
      expect(frame.canReceiveRolls()).to.eq(false);
      const strikeFrame = new Frame(2);
      strikeFrame.inputRollScore(10);
      expect(strikeFrame.canReceiveRolls()).to.eq(false);
    });

    it("Frame: get rolls", function() {
      const frame = new Frame(2);
      frame.inputRollScore(4);
      frame.inputRollScore(5);
      expect(frame.getFirstRoll()).to.be.a("number");
      expect(frame.getFirstRoll()).to.eq(4);
      expect(frame.getSecondRoll()).to.be.a("number");
      expect(frame.getSecondRoll()).to.eq(5);
      const strikeFrame = new Frame(2);
      strikeFrame.inputRollScore(10);
      expect(strikeFrame.getFirstRoll()).to.eq(10);
      expect(strikeFrame.getSecondRoll()).to.eq(undefined);
    });

    describe("Frame error messages", function() {
      it("Frame Error: should throw error if putting in more than max pins", function() {
        const errorMessage = "The score is higher than max number of pins 10";
        const frame = new Frame (2);
        const badFn = () => { frame.inputRollScore(11); };
        expect(badFn).to.throw(errorMessage);
      });

      it("Frame Error: should throw error if attempting to input roll when rolls have been maxed", function() {
        const errorMessage = "The frame has received max rolls of 2";
        const frame = new Frame (2);
        frame.inputRollScore(3);
        frame.inputRollScore(4);
        const badFn = () => {  frame.inputRollScore(2); };
        expect(badFn).to.throw(errorMessage);
      });

      it("Frame Error: should throw error if attempting to input roll where the sum is greater than max pins", function() {
        const errorMessage = "Can not throw a score of 7 when there are only 5 pins remaining";
        const frame = new Frame (2);
        frame.inputRollScore(5);
        const badFn = () => {  frame.inputRollScore(7); };
        expect(badFn).to.throw(errorMessage);
      });
    });
  });
};

export { frameTests };
