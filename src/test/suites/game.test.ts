import * as mocha from "mocha";
import chai from "chai";
import { bowl } from "../../scoreboard";
import { BowlingGameResults } from "../../BowlingGame";

const expect = chai.expect;

const gameResultsTests = function() {
  describe("Game Results Tests", function() {


    it("Should return correct results", function() {
      const scores = "10, 10, 10, 10, 0, 4, 1, 6, 1, 7, 3, 7, 2, 0, 10, 10, 5";
      const  { results }  = bowl(scores);
      expect(results).to.be.a("string");
      expect(results).to.eq("|X   |X   |X   |X   |-, 4|1, 6|1, 7|3, /|2, -|X, X, 5|");
    });

    it("Should return the correct results, not including the final 7", function() {
      const scores = "10, 9, 1, 5, 3, 2, 4, 1, 4, 5, 4, 0, 8, 2, 7, 6, 1, 0, 9, 7";
      const { results } = bowl(scores);
      expect(results).to.be.a("string");
      expect(results).to.eq("|X   |9, /|5, 3|2, 4|1, 4|5, 4|-, 8|2, 7|6, 1|-, 9   |");
    });
  });
};

export { gameResultsTests };
