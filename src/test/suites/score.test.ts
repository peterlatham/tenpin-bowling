import * as mocha from "mocha";
import chai from "chai";
import { bowl } from "../../scoreboard";

const expect = chai.expect;

const scoreTests = function() {
  describe("Scoring Tests", function() {
    it("Should return a zero score", function() {
      const scores = "0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0";
      const { score } = bowl(scores);
      expect(score).to.be.a("number");
      expect(score).to.eq(0);
    });

    it("Should return a simple score of only the first frame", function() {
      const scores = "3, 6, 0, 0, 0";
      const { score } = bowl(scores);
      expect(score).to.be.a("number");
      expect(score).to.eq(9);
    });

    it("Should return a score added after a spare", function() {
      const scores = "3, 7, 6, 0, 0";
      const { score } = bowl(scores);
      expect(score).to.be.a("number");
      expect(score).to.eq(22);
    });

    it("Should return 152", function() {
      const scores = "10, 10, 10, 10, 0, 4, 1, 6, 1, 7, 3, 7, 2, 0, 10, 10, 5";
      const  { score }  = bowl(scores);
      expect(score).to.be.a("number");
      expect(score).to.eq(152);
    });

    it("Should return 96", function() {
      const scores = "10, 9, 1, 5, 3, 2, 4, 1, 4, 5, 4, 0, 8, 2, 7, 6, 1, 0, 9, 7";
      const { score } = bowl(scores);
      expect(score).to.be.a("number");
      expect(score).to.eq(96);
    });

    it("Should return 168", function() {
      const scores = "1, 9, 5, 5, 8, 2, 9, 1, 4, 6, 4, 6, 8, 2, 7, 3, 9, 1, 9, 1, 5";
      const { score } = bowl(scores);
      expect(score).to.be.a("number");
      expect(score).to.eq(168);
    });


    it("Shoud return a near perfect score - 299", function() {
      it("Should return a perfect score", function() {
        const scores = "10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 9";
        const { score } = bowl(scores);
        expect(score).to.be.a("number");
        expect(score).to.eq(299);
      });
    });

    it("Should return a perfect score", function() {
      const scores = "10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10";
      const { score } = bowl(scores);
      expect(score).to.be.a("number");
      expect(score).to.eq(300);
    });
  });
};

export { scoreTests };
