import * as mocha from "mocha";
import chai from "chai";
import { bowl } from "../../scoreboard";

const expect = chai.expect;

const headerTests = function() {
  describe("Header Tests", function() {
    it("A game result should print out the correct headers", function() {
      const scores = "10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10";
      const { headers } = bowl(scores);
      expect(headers).to.be.a("string");
      expect(headers.length).to.eq(54);
      expect(headers).to.eq("| f1 | f2 | f3 | f4 | f5 | f6 | f7 | f8 | f9 | f10   |");
    });
  });
};

export { headerTests };
