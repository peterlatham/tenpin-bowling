import * as mocha from "mocha";
import chai from "chai";
import { bowl } from "../../scoreboard";
import { FinalFrame } from "../../FinalFrame";

const expect = chai.expect;

const finalFrameTests = function() {
  describe("FinalFrame Tests", function() {

    it("Frame: Can recieve rolls increased to 3 under certain conditions", function() {
      const frame = new FinalFrame(11);
      frame.inputRollScore(4);
      expect(frame.canReceiveRolls()).to.be.a("boolean");
      expect(frame.canReceiveRolls()).to.eq(true);
      frame.inputRollScore(5);
      expect(frame.canReceiveRolls()).to.eq(false);
      const strikeFrame = new FinalFrame(11);
      strikeFrame.inputRollScore(10);
      expect(strikeFrame.canReceiveRolls()).to.eq(true);
      strikeFrame.inputRollScore(10);
      expect(strikeFrame.canReceiveRolls()).to.eq(true);
      strikeFrame.inputRollScore(10);
      expect(strikeFrame.canReceiveRolls()).to.eq(false);
      const spareFrame = new FinalFrame(11);
      spareFrame.inputRollScore(5);
      expect(spareFrame.canReceiveRolls()).to.eq(true);
      spareFrame.inputRollScore(5);
      expect(spareFrame.canReceiveRolls()).to.eq(true);
      spareFrame.inputRollScore(3);
      expect(spareFrame.canReceiveRolls()).to.eq(false);
    });

    const irrelevantScores = "0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,";

    describe("Final Frame Output results display", function() {
      it("Should return a spare only and then a miss", function() {
        const scores = irrelevantScores + " 1, 9, 0";
        const { results } = bowl(scores);
        expect(results).to.be.a("string");
        expect(results).to.contain("|1, /, -|");
      });

      it("Should return a single first roll", function() {
        const scores = irrelevantScores + "  1, 0, 0";
        const { results } = bowl(scores);
        expect(results).to.be.a("string");
        expect(results).to.contain("|1, -   |");
      });

      it("Should return a single second roll, and nothing in the third roll", function() {
        const scores = irrelevantScores + " 0, 9, 0";
        const { results } = bowl(scores);
        expect(results).to.be.a("string");
        expect(results).to.contain("|-, 9   |");
      });

      it("Should return a spare on second roll", function() {
        const scores = irrelevantScores + " 0, 10, 0";
        const { results } = bowl(scores);
        expect(results).to.be.a("string");
        expect(results).to.contain("|-, /, -|");
      });

      it("Should return a spare on second roll, and strike on the last", function() {
        const scores = irrelevantScores + " 0, 10, 10";
        const { results } = bowl(scores);
        expect(results).to.be.a("string");
        expect(results).to.contain("|-, /, X|");
      });

      it("Should return a spare and strike in the third attempt", function() {
        const scores = irrelevantScores + " 6, 4, 10";
        const { results } = bowl(scores);
        expect(results).to.be.a("string");
        expect(results).to.contain("|6, /, X|");
      });

      it("Should return a strike and a spare in the last frame", function() {
        const scores = irrelevantScores + " 10, 1, 9";
        const { results } = bowl(scores);
        expect(results).to.be.a("string");
        expect(results).to.contain("|X, 1, /|");
      });

      it("Should return a strike, a miss and a spare in the last frame", function() {
        const scores = irrelevantScores + " 10, 0, 10";
        const { results } = bowl(scores);
        expect(results).to.be.a("string");
        expect(results).to.contain("|X, -, /|");
      });

      it("Should return two strikes in succession in the last frame", function() {
        const scores = irrelevantScores + " 10, 10, 0";
        const { results } = bowl(scores);
        expect(results).to.be.a("string");
        expect(results).to.contain("|X, X, -|");
      });
    });

  });
};

export { finalFrameTests };
