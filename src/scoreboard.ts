
import { BowlingGame, BowlingGameResults } from "./BowlingGame";

const bowl = (res: string): BowlingGameResults => {
  const game = new BowlingGame();
  // input is always assumed valid
  game.fillScores((res !== "") ? res.split(" ").join("").split(",") : [] );
  const results = game.getResults();
  return results;
};

export { bowl };